# mir-sc-common

- `mir-sc-common`: scala commons library using tapir 1.7
- `mir-sc-common-tapir`: scala commons library using tapir 1.2.3

("mir" stands for "mirroring" mechanism in gitlab)

---
some new lines here (11:14)
---

## Updating

When the mirror repository is updated, all new branches, tags, and commits are visible in the project’s activity feed. A repository mirror at GitLab updates automatically.
(https://docs.gitlab.com/ee/user/project/repository/mirror/)

### mir-sc-common -> mir-sc-common-tapir: PULL

- Change settings in `mir-sc-common-tapir`, Repository / Mirroring = **PULL**
- PULL value in Direction field is read-only ...

**"Pull from a remote repository" is only available for PREMIUM**.
(Moved to GitLab Premium in 13.9.)
https://docs.gitlab.com/ee/user/project/repository/mirror/pull.html

### mir-sc-common -> mir-sc-common-tapir: PUSH

- Change settings in `mir-sc-common`: Repository / Mirroring = **PUSH**
- When configuring mirroring, user need permissions
- User also needs to create a PAT (personal access token) with `write_repository` scope
- Update process (mirroring) is automated (not sure about frequency)
- But also can be triggered manually (5 min in between)

After new commits in `mir-sc-common`, updates are copied/reflected without problems in `mir-sc-common-tapir`.
**BUT, if new updates are commited in `mir-sc-common-tapir`, mirroring will FAIL.**
After failing I was unable to setup the mirroring process again:

    - retries fail
    - adding a new mirroring process in Setting won't work either

### mir-sc-common <- mir-sc-common-tapir

Usually we don't need it, except when:

- all services are migrated to the latest tapir version
- we need to move all changes to `scala-commons`

### mir-sc-common <-> mir-sc-common-tapir (Bidirectional mirroring)

**"Bidirectional mirroring" is only available for PREMIUM**.
(Moved to GitLab Premium in 13.9.)
https://docs.gitlab.com/ee/user/project/repository/mirror/bidirectional.html

## Cases

### Both `master` branches are in sync in both projects

Then new updates in `master` in `mir-sc-common`.
Project `mir-sc-common-tapir` was updated successfully.

### New commits in `master` branch in `mir-sc-common-tapir`

**Update failed**

Retrying didn't solve the issue.
Configuring a new mirroring process didn't solve the issue neither.
